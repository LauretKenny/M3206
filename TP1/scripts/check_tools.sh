#!/bin/bash

if dpkg -l git >/dev/null; then
echo "[...] git : installé [...]"
else
echo "[/!\] git : pas installé [/!\] lancer la commande : 'apt-get install git' "
fi

if dpkg -l tmux >/dev/null; then
echo "[...] tmux : installé [...]"
else
echo "[/!\] tmux : pas installé [/!\] lancer la commande : 'apt-get install tmux' "
fi

if dpkg -l vim >/dev/null; then
echo "[...] vim : installé [...]"
else
echo "[/!\] vim : pas installé [/!\] lancer la commande : 'apt-get install vim' "
fi

if dpkg -l htop >/dev/null; then
echo "[...] htop : installé [...]"
else
echo "[/!\] htop : pas installé [/!\] lancer la commande : 'apt-get install htop' "
fi
