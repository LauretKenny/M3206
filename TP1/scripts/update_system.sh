#!/bin/bash

if [ "$UID" == "O" ]
then
	echo "[/!\] Vous devez etre etre super-utilisateur [/!\]"
	exit 1
else
	echo "[...] update database [...]"
	apt-get update >/dev/null
	apt-get upgrade >/dev/null
	echo "[...] upgrade system [...]"
fi
