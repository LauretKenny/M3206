#!/bin/bash

if dpkg -l ssh >/dev/null; then
	echo "[...] ssh : est installé [...]"
else 
	echo "[...] ssh : est pas installé [...]"
fi

if ps -e | grep ssh >/dev/null; then
	echo "[...] ssh : fonctionne [...]"
else
	echo "[/!\] ssh : les service n'est pas lancé [/!\]"
	echo "[...] ssh : lancement du service  [...] lancer la commande : /etc/init.d/sshd start"
fi
