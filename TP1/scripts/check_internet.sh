#!/bin/bash

echo "[...] Checking internet connection [...]"

if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then  //
	echo "[...] Internet acces OK [...]"
else
	echo "[/!\ Not connected to Internet /!\][]"
	echo "[/!\] Please check configuration [/!\]"
fi
