#!/bin/bash

#La date 
backupdate=$(date +%Y-%m-%d) 
nom=$backupdate-User.tar.gz

#Archiver un dossier 
tar cvzf /tmp/backup/$backupdate-User.tar.gz $1 > /dev/null

#Verifier si le fichier tar.gz existe 
if [ -e /tmp/backup/$backupdate-User.tar.gz ]; then
	echo "Creation de l'archive : "$nom
else 
	echo "No"
fi

